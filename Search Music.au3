#include <IE.au3>
#include <GuiButton.au3>
#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <GuiImageList.au3>
#include <GuiButton.au3>

Opt("CaretCoordMode", 2)
Opt("MouseCoordMode", 2)
Opt("PixelCoordMode", 2)
Opt("GUIOnEventMode", 1)

Global $firefoxDir = "C:\Program Files (x86)\Mozilla Firefox"


Global $guiWidth = 240
Global $guiHeight = 115
Global $hGui = GUICreate("Search Music", $guiWidth,$guiHeight,0,0)


GUICtrlCreateLabel("Artist:", 10, 23, 30,20)
Global $hArtist = GUICtrlCreateInput("",40, 20, 150,20)
GUICtrlCreateLabel("Song:",10,53,30,20)
Global $hSong = GUICtrlCreateInput("",40, 50, 150, 20)

;~ GUICtrlCreateLabel("Quotes", 195, 5, 35, 20)
GUICtrlCreateGroup("Quotes", 195, 5, 35, 70)
$hQuotesArtist = GUICtrlCreateCheckbox("",205, 20, 20, 20)
GUICtrlSetState(-1,1)
$hQuotesSong = GUICtrlCreateCheckbox("",205, 50, 20, 20)
GUICtrlSetState(-1,1)

GUICtrlCreateGroup("Browser:",10,75,180,35)
$hFireFox = GUICtrlCreateRadio("",20,87,40,20, $BS_BITMAP)
_GUICtrlButton_SetImage($hFireFox, @WorkingDir &"\FireFoxIcon.bmp")
GUICtrlSetState(-1,$GUI_CHECKED)
$hIExplorer = GUICtrlCreateRadio("",65,87,40,20, $BS_BITMAP)
_GUICtrlButton_SetImage($hIExplorer,@WorkingDir &"\IExplorerIcon.bmp", 16, True)


GUICtrlCreateButton("Search", $guiWidth-40, $guiHeight-30,40,30, $BS_DEFPUSHBUTTON)
GUICtrlSetOnEvent(-1,"search")

GUISetOnEvent($GUI_EVENT_CLOSE, "Terminate")

GUISetState(@SW_SHOW)



Global $base = "https://www.google.ca/search?q=%3Fintitle:index.of%3F+mp3+"
;~ Global $link = "%3Fintitle:index.of%3F+mp3+%22mac+miller%22+%22don't+mind%22&rls=com.microsoft:en-CA%3AIE-Address"
Global $artist
Global $song
Global $endIE = "&rls=com.microsoft:en-CA%3AIE-Address"
;~ Global $endFireFox = "https://www.google.ca/search?q=intitle:index.of%3F+mp3+%22john+lennon%22&ie=utf-8&oe=utf-8&rls=org.mozilla:en-US:official&client=firefox-a&channel=fflb&gws_rd=cr&ei=4qdIUsWYOZSCqQHQl4CQDQ
Global $endFireFox = "&ie=utf-8&oe=utf-8&rls=org.mozilla:en-US:official&client=firefox-a&channel=fflb&gws_rd=cr&ei=4qdIUsWYOZSCqQHQl4CQDQ"
Global $link
;~ Global $oIE = _IECreate($link)

While (1)
	sleep(50)
WEnd

Func search()
	local $arty = parseWord(GUICtrlRead($hArtist))
	If GUICtrlRead($hQuotesArtist) == $GUI_CHECKED Then
		$arty = "%22" & $arty & "%22"
	EndIf
	local $songy = parseWord(GUICtrlRead($hSong))
	If GUICtrlRead($hQuotesSong) == $GUI_CHECKED Then
		$songy= "%22" & $songy & "%22"
	EndIf
	If GUICtrlRead($hFireFox) == $GUI_CHECKED Then
		Run($firefoxDir & "\firefox.exe -new-window "& $base & $arty & "+" & $songy & $endFireFox, $firefoxDir)
	ElseIf GUICtrlRead($hIExplorer) == $GUI_CHECKED Then
		$link = $base & $arty & "+" & $songy & $endIE
		_IECreate($link, 0, 1, 0)
	EndIf
	Exit
;~ 	ConsoleWrite($link & @CRLF)
;~ 	_IECreate($link)
EndFunc

Func parseWord($str)
	$str = StringReplace($str, " ", "+")
;~ 	$str = StringReplace($str, '"', "%22")
	return $str
EndFunc

Func Terminate()
	Exit
EndFunc